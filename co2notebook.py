import pandas as pd
import numpy as np
import boto3
import matplotlib.pyplot as plt

client = boto3.client('iotanalytics')
dataset = "row_data"
dataset_url = client.get_dataset_content(datasetName = dataset)['entries'][0]['dataURI']
data = pd.read_csv(dataset_url)
data = data.sort_values(by='timestep_time')

plt.plot(data['timestep_time'], data['vehicle_co2'])
plt.show()

plt.plot(data['timestep_time'], data['vehicle_noise'])
plt.show()

plt.plot(data['timestep_time'], data['vehicle_co'])
plt.show()
