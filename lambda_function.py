import json
import logging
import sys

import greengrasssdk

# Logging
logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

# SDK Client
client = greengrasssdk.client("iot-data")

# Counter
my_counter = 0
def function_handler(event, context):
    global my_counter
    #TODO1: Get your data
    print(event)
    co2 = event['vehicle_CO2']
    print(co2)
    co2m = max(co2.values())
    #TODO2: Calculate max CO2 emission
    #data = co2
    data =json.dumps({"co2": co2m})

    #TODO3: Return the result
    client.publish(
        topic="co2_data",
        payload=data,
    )
    my_counter += 1
    return
